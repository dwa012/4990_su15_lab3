class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :body, null: false
      t.integer :votes, default: 0, null: false
      t.references :user, null: false

      t.timestamps null: false
    end

    add_foreign_key :posts, :users
  end
end
