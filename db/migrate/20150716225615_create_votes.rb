class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :entity_id
      t.string :entity_type
      t.references :user

      t.timestamps null: false
    end

    add_foreign_key :votes, :users
    add_index :votes, [:entity_id, :entity_type, :user_id], unique: true
    add_index :votes, [:entity_id, :entity_type]
  end
end
