# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

%w(Cleaner Sponges Toys).each do |name|
  Product.where(name: name).first_or_create!
end

[
    {
        name: 'Cleaner',
        quantity: 10
    },
    {
        name: 'Toys',
        quantity: 20
    }
].each do |product|
  Product.where(name: product[:name], quantity: product[:quantity]).first_or_create!
end

User.where(email: 'dwa012@gmail.com').first_or_create!


