require "rails_helper"

RSpec.describe SessionController, type: :routing do
  describe "routing" do

    it "routes to #login" do
      expect(:get => "/session/login").to route_to("session#login")
    end

    it "routes to #logout" do
      expect(:delete => "/session/logout").to route_to("session#logout")
    end

    it "routes to #create" do
      expect(:post => "/session/create").to route_to("session#create")
    end

  end
end
