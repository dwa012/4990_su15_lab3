# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  body       :text             not null
#  votes      :integer          default(0), not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Post, type: :model do
  it 'has a valid factory' do
    expect(build(:post)).to be_valid
  end

  it 'is invalid without user' do
    expect(build(:post, :without_user)).to_not be_valid
  end

  it 'is invalid without a body' do
    expect(build(:post, body: nil)).to_not be_valid
  end

  it 'is no votes initially' do
    expect(create(:post).votes.count).to eq(0)
  end

end
