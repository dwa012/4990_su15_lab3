# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  body       :text             not null
#  post_id    :integer          not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Comment, type: :model do
  it 'has a valid factory with traits' do
    expect(build(:comment, :with_user, :with_post)).to be_valid
  end

  it 'is invalid without body' do
    expect(build(:comment, :with_user, :with_post, body: nil)).to_not be_valid
  end

  it 'is invalid without user' do
    expect(build(:comment, :with_post)).to_not be_valid
  end

  it 'is invalid without post' do
    expect(build(:comment, :with_user)).to_not be_valid
  end


  it 'is no votes initially' do
    expect(create(:comment).votes.count).to eq(0)
  end
end
