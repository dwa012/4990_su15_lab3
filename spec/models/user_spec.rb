# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  first_name      :string
#  last_name       :string
#  email           :string
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create(:user) }

  it 'has a valid factory' do
    expect(build(:user)).to be_valid
  end

  it 'is invalid with bad email format' do
    expect(build(:user, email: 'example@example')).to_not be_valid
  end

  it 'has one post' do
    create(:post, user: user)
    expect(user.posts.count).to eq(1)
  end

  it 'has one comment' do
    create(:comment, user: user, post: create(:post, user: user))
    expect(user.comments.count).to eq(1)
  end
end
