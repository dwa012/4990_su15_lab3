# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  body       :text             not null
#  post_id    :integer          not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :comment do
    body FFaker::HipsterIpsum.paragraph

    trait :with_post do
      association :post, factory: :post
    end

    trait :with_user do
      association :user, factory: :user
    end
  end

end
