# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  first_name      :string
#  last_name       :string
#  email           :string
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :user do
    first_name FFaker::Name.first_name
    last_name FFaker::Name.last_name
    email FFaker::Internet.email
    password 'password123'
    password_confirmation 'password123'
  end

end
