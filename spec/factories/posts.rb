# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  body       :text             not null
#  votes      :integer          default(0), not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :post do
    body FFaker::HipsterIpsum.paragraph

    association :user, factory: :user

    trait :without_user do
      user nil
    end
  end

end
