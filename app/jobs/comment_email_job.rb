class CommentEmailJob < ActiveJob::Base
  queue_as :email

  def perform(comment)
    Notifications.new_comment(comment).deliver_now
  end
end
