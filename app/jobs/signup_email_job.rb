class SignupEmailJob < ActiveJob::Base
  queue_as :email

  def perform(user)
    Notifications.signup(user).deliver_now
  end
end
