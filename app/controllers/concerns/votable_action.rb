module VotableAction
  extend ActiveSupport::Concern

  included do
    before_action :set_entity, only: [:upvote, :downvote]
  end

  def upvote
    @entity.add_vote(current_user)

    respond_to do |format|
      format.js
    end

  end

  def downvote
    @entity.remove_vote(current_user)

    respond_to do |format|
      format.js { render 'upvote'}
    end
  end

  private

  def set_entity
    @entity = controller_name.classify.constantize.find(params[:id])
  end

  def entity_name
    controller_name.classify.constantize.to_s
  end

end