class SessionController < ApplicationController
  def login
  end

  def create
    user = User.where(email: params[:email].downcase).first

    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_path, notice: 'Logged in successfully'

    else
      redirect_to session_login_path, notice: 'Error logging in'

    end

  end

  def logout
    session[:user_id] = nil
    redirect_to root_path, notice: 'Logged out successfully'
  end
end
