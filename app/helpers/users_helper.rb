module UsersHelper
  # Returns the Gravatar for the given user.
  #
  # @param [User] user
  # @param [Hash] options
  def gravatar_for(user, options = {})
    options = {
        size: '200',
        default_icon: 'identicon',
        rating: 'pg'
    }.deep_merge(options)

    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?r=#{options[:rating]}&s=#{options[:size]}&d=#{options[:default_icon]}"
    link_to image_tag(gravatar_url, alt: user.first_name, class: "gravatar"), public_users_path(user)
  end
end
