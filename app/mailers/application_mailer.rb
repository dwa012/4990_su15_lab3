class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@awesomesite.biz'
  layout 'mailer'
end
