class Notifications < ApplicationMailer

  def signup(user)
    @user = user

    mail to: @user.email
  end

  def new_comment(comment)
    @comment = comment

    mail to: @comment.post.user.email
  end
end
