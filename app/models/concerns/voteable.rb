module Voteable
  extend ActiveSupport::Concern

  included do
    has_many :votes, as: :entity
  end

  def add_vote(user)
    Vote.create!(entity: self, user: user)
  end

  def remove_vote(user)
    self.votes.where(user: user).first.destroy
  end
end