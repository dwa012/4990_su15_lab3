# == Schema Information
#
# Table name: votes
#
#  id          :integer          not null, primary key
#  entity_id   :integer
#  entity_type :string
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Vote < ActiveRecord::Base
  belongs_to :user
  belongs_to :entity, polymorphic: true

  validates_uniqueness_of :entity_id, scope: [:entity_type, :user_id]
end
