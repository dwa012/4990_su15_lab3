# == Schema Information
#
# Table name: relationships
#
#  id          :integer          not null, primary key
#  follower_id :integer          not null
#  followed_id :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Relationship < ActiveRecord::Base
  belongs_to :follower, foreign_key: :follower_id, class_name: 'User', primary_key: :id
  belongs_to :followed, foreign_key: :followed_id, class_name: 'User', primary_key: :id

  validates_presence_of :follower
  validates_presence_of :followed

  validates_uniqueness_of :follower, scope: [:followed]
end
