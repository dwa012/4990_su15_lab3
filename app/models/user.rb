# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  first_name      :string
#  last_name       :string
#  email           :string
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User < ActiveRecord::Base
  has_secure_password

  has_many :posts
  has_many :comments
  has_many :votes

  has_many :active_relationships, class_name: 'Relationship', foreign_key: :follower_id, dependent: :destroy
  has_many :passive_relationships, class_name: 'Relationship', foreign_key: :followed_id, dependent: :destroy

  has_many :followers, through: :passive_relationships, source: :follower
  has_many :following, through: :active_relationships, source: :followed

  validates_presence_of :first_name
  validates_presence_of :last_name
  validates_presence_of :email

  validates_format_of :email, with: /\A[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]+\z/

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def abbreviated_name
    self.first_name[0].upcase + '. ' + self.last_name
  end

  def feed
    Post.where('user_id = ? OR user_id IN (?)', self.id, self.following.ids).order('created_at DESC')
  end
end
