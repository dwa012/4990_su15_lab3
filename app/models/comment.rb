# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  body       :text             not null
#  post_id    :integer          not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Comment < ActiveRecord::Base
  include Voteable

  belongs_to :user
  belongs_to :post

  validates_presence_of :body
  validates_presence_of :post
  validates_presence_of :user
end
