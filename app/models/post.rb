# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  body       :text             not null
#  votes      :integer          default(0), not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Post < ActiveRecord::Base
  include Voteable

  belongs_to :user
  has_many :comments

  validates_presence_of :body
  validates_presence_of :user

  def comments_sorted_by_votes
    self.comments.sort {|c1, c2| c2.votes.count <=> c1.votes.count }
  end
end
