json.array!(@posts) do |post|
  json.extract! post, :id, :body, :votes
  json.url post_url(post, format: :json)
end
